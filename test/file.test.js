'use strict';

import { getSize, getHash, getContent } from '../lib/file.js';

describe('file', () => {
  const filePath = './test/files/lorem-ipsum-copy.txt';
  const emptyFilePath = './test/files/folder/subfolder/empty-file';

  describe('size', () => {
    it('should obtain the size of the file', async () => {
      const fileSize = await getSize(filePath);
      expect(fileSize).toBe('11');
    });

    it('should obtain the size of the empty file', async () => {
      const emptyFileSize = await getSize(emptyFilePath);
      expect(emptyFileSize).toBe('0');
    });
  });

  describe('hash', () => {
    it('should calculate the MD5 hash of the file', async () => {
      const fileHash = await getHash(filePath);
      expect(fileHash).toBe('0956d2fbd5d5c29844a4d21ed2f76e0c');
    });

    it('should calculate the MD5 hash of the empty file', async () => {
      const emptyFileHash = await getHash(emptyFilePath);
      expect(emptyFileHash).toBe('d41d8cd98f00b204e9800998ecf8427e');
    });
  });

  describe('content', () => {
    it('should obtain the content buffer of the file', async () => {
      const fileContent = await getContent(filePath);
      expect(fileContent).toBeInstanceOf(Buffer);
      expect(fileContent.toString()).toBe('Lorem ipsum');
    });

    it('should obtain the content buffer of the empty file', async () => {
      const emptyFileContent = await getContent(emptyFilePath);
      expect(emptyFileContent).toBeInstanceOf(Buffer);
      expect(emptyFileContent.toString()).toBe('');
    });
  });
});