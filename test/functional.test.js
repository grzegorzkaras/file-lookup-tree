'use strict';

import { lazySequencer, stringify, andThenMultiple } from '../lib/utils/functional.js';

describe('functional', () => {
  describe('lazySequencer', () => {
    let first;
    let second;
    let third;

    beforeEach(() => {
      first = jest.fn();
      second = jest.fn();
      third = jest.fn();
      
      first.mockReturnValue('argument 1');
      second.mockReturnValue('argument 2');
      third.mockReturnValue('argument 3');
    });

    it('should evaluate in lazy manner', () => {
      const sequencerIterator = lazySequencer([first, second, third])('argument A', 'argument B');

      expect(sequencerIterator.next()).toEqual({ value: 'argument 1', done: false });
      expect(first).toHaveBeenCalledWith('argument A', 'argument B');

      expect(sequencerIterator.next()).toEqual({ value: 'argument 2', done: false });
      expect(second).toHaveBeenCalledWith('argument A', 'argument B');

      expect(sequencerIterator.next()).toEqual({ value: 'argument 3', done: true });
      expect(third).toHaveBeenCalledWith('argument A', 'argument B');
    });

    it('should execute without any initial arguments', () => {
      const sequencerIterator = lazySequencer([first, second, third])();

      expect(sequencerIterator.next()).toEqual({ value: 'argument 1', done: false });
      expect(first).toHaveBeenCalledWith();

      expect(sequencerIterator.next()).toEqual({ value: 'argument 2', done: false });
      expect(second).toHaveBeenCalledWith();

      expect(sequencerIterator.next()).toEqual({ value: 'argument 3', done: true });
      expect(third).toHaveBeenCalledWith();
    });
  });

  describe('stringify', () => {
    it('should turn an object into a string with indentation of 2 spaces', () => {
      const result = stringify({ 
        a: 10, 
        b: {
          c: 11,
          d: 12
        }
      });

      expect(result).toBe([
        '{',
        '  "a": 10,',
        '  "b": {',
        '    "c": 11,',
        '    "d": 12',
        '  }',
        '}'
      ].join('\n'));
    });
  });

  describe('andThenMultiple', () => {
    let onSuccess;

    beforeEach(() => {
      onSuccess = jest.fn((a, b, c) => `First argument: ${a}, Second argument: ${b}, Third argument: ${c}`);
    });

    it('should take promises as arguments and call function with values returned from them', async () => {
      const result = await andThenMultiple(onSuccess)(Promise.resolve(10), 11, Promise.resolve('value'));

      expect(onSuccess).toHaveBeenCalledWith(10, 11, 'value');
      expect(result).toBe('First argument: 10, Second argument: 11, Third argument: value');
    });

    it('should take regular arguments and still call function with those arguments intact', async () => {
      const result = await andThenMultiple(onSuccess)(10, 11, Promise.resolve('value'));

      expect(onSuccess).toHaveBeenCalledWith(10, 11, 'value');
      expect(result).toBe('First argument: 10, Second argument: 11, Third argument: value');
    });
  });
});