'use strict';

import { resolve, join } from 'path';

import { readFile, writeFile } from 'atomically';

import { FileLookupError } from '../lib/FileLookupError.js';

import * as FileLookupTree from '../lib/FileLookupTree.js';
import * as FileIdentifier from '../lib/FileIdentifier.js';

jest.mock('atomically');

describe('FileLookupTree', () => {
  const folderPath = resolve('./test/files/folder/');
  const filePathList = [join(folderPath, 'lorem-ipsum.txt'), join(folderPath, 'hash-collision.jpg'), join(folderPath, 'subfolder/empty-file')];

  const newFile = resolve('./test/files/new-file.txt');
  const collisionFile = resolve('./test/files/hash-collision.jpg');
  const newFileOfExistingSize = resolve('./test/files/lorem-ipsum-reversed.txt');
  const nonExistingFile = resolve('./test/files/non-existing-file.txt');

  const fileLookupTreeSavePath = resolve('./test/temp/file-lookup-tree.json');

  // Both should have the same size and hash, but not content
  const includedFile = resolve('./test/files/folder/hash-collision.jpg');
  const notIncludedFile = resolve('./test/files/hash-collision.jpg');

  describe('create', () => {
    describe('fromFolder', () => {
      it(`should create a FileLookupTree from folder and it's subfolders`, async () => {
        const fileLookupTree = await FileLookupTree.create(folderPath);

        expect(fileLookupTree).toEqual({ 
          "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
          "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
          "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
        });
      });
    });
    
    describe('fromFileList', () => {
      it(`should create a FileLookupTree from file list`, async () => {
        const fileLookupTree = await FileLookupTree.create(filePathList);

        expect(fileLookupTree).toEqual({ 
          "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
          "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
          "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
        });
      });
    });
  });

  describe('save', () => {
    it('should save FileLookupTree to a file', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);

      writeFile.mockResolvedValue(Promise.resolve());

      await FileLookupTree.save(fileLookupTreeSavePath, fileLookupTree);

      expect(writeFile).toBeCalledWith(fileLookupTreeSavePath, JSON.stringify(fileLookupTree, null, '  '), expect.any(Object));
    });
  });

  describe('load', () => {
    it('should load FileLookupTree from a file', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);

      readFile.mockResolvedValue(JSON.stringify(fileLookupTree, null, '  '));

      const loadedFileLookupTree = await FileLookupTree.load(fileLookupTreeSavePath);

      expect(readFile).toBeCalledWith(fileLookupTreeSavePath);
      expect(loadedFileLookupTree).toEqual(fileLookupTree);
    });
  });

  describe('add', () => {
    it('should add new file entry with existing size and hash to a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);

      const newFileLookupTree = await FileLookupTree.add(fileLookupTree, collisionFile);

      expect(newFileLookupTree).toEqual({ 
        "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1], collisionFile] }, 
        "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
      });
    });

    it('should add new file entry with new size and hash to a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);

      const newFileLookupTree = await FileLookupTree.add(fileLookupTree, newFile);

      expect(newFileLookupTree).toEqual({ 
        "16":     { "f2f0509f347fa292d14cc771cb59f2c2": [newFile] },
        "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
        "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
      });
    });

    it('should add new file entry with existing size but different hash to a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);

      const newFileLookupTree = await FileLookupTree.add(fileLookupTree, newFileOfExistingSize);

      expect(newFileLookupTree).toEqual({ 
        "11": {
          "9c77a5f0be3981027d7e20e8d386e1bd": [newFileOfExistingSize],
          "0956d2fbd5d5c29844a4d21ed2f76e0c":  [filePathList[0]]
        }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
        "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
      });
    });
  });

  describe('remove', () => {
    it('should remove unique file entry from a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create(filePathList);

      const newFileLookupTree = await FileLookupTree.remove(fileLookupTree, filePathList[2]);

      expect(newFileLookupTree).toEqual({  
        "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }
      });
    });

    it('should remove file entry with hash collision from a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create([...filePathList, collisionFile]);

      const newFileLookupTree = await FileLookupTree.remove(fileLookupTree, collisionFile);

      expect(newFileLookupTree).toEqual({  
        "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
        "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
      });
    });

    it('should remove same size file entry from a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create([...filePathList, newFileOfExistingSize]);

      const newFileLookupTree = await FileLookupTree.remove(fileLookupTree, newFileOfExistingSize);

      expect(newFileLookupTree).toEqual({  
        "11":     { "0956d2fbd5d5c29844a4d21ed2f76e0c": [filePathList[0]] }, 
        "335104": { "253dd04e87492e4fc3471de5e776bc3d": [filePathList[1]] }, 
        "0":      { "d41d8cd98f00b204e9800998ecf8427e": [filePathList[2]] }
      });
    });
  });

  describe('exists', () => {
    it('should return true if file exists in a FileLookupTree', async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);
      const fileIdentifier = FileIdentifier.create(includedFile);

      expect(await FileLookupTree.exists(fileLookupTree, fileIdentifier)).toBe(true);
    });

    it(`should return false if file doesn't exists in a FileLookupTree`, async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);
      const fileIdentifier = FileIdentifier.create(notIncludedFile);

      expect(await FileLookupTree.exists(fileLookupTree, fileIdentifier)).toBe(false);
    });
    
    it(`should return false if size of the file doesn't exists in a FileLookupTree`, async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);
      const fileIdentifier = FileIdentifier.create(newFile);

      expect(await FileLookupTree.exists(fileLookupTree, fileIdentifier)).toBe(false);
    });
    
    it(`should return false if the size exists but hash of the file doesn't exists in a FileLookupTree`, async () => {
      const fileLookupTree = await FileLookupTree.create(folderPath);
      const fileIdentifier = FileIdentifier.create(newFileOfExistingSize);

      expect(await FileLookupTree.exists(fileLookupTree, fileIdentifier)).toBe(false);
    });
    
    it(`should throw FileLookupError if the file in the tree cannot be accessed`, async () => {
      const fileLookupTree = await FileLookupTree.create([newFile]);
      const fileIdentifier = FileIdentifier.create(newFile);

      fileLookupTree['16']['f2f0509f347fa292d14cc771cb59f2c2'][0] = nonExistingFile;
      
      const existsPromise = FileLookupTree.exists(fileLookupTree, fileIdentifier);

      await expect(existsPromise).rejects.toBeInstanceOf(FileLookupError);
    });
  });
});