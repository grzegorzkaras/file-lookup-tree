'use strict';

import { resolve, dirname, basename, sep } from 'path';

import { getArgumentFilepath, setupBenchmark, measureAsync, getFolderSize, getFileSize } from './benchmark.js';

import { FileLookupTree, FileIdentifier } from '../../index.js';

const filepath = getArgumentFilepath();

const paths = {
  fileLookupTreeSave: resolve('./test/temp/file-lookup-tree.json'),
  file: filepath,
  folder: dirname(filepath)
};

setupBenchmark();

console.log('\n\x1b[30m\x1b[42m Beginning performance test: \x1b[0m\n');

const fileIdentifier = await measureAsync(
  'FileIdentifier.create', 
  () => FileIdentifier.create(paths.file)
);

const fileLookupTree = await measureAsync(
  'FileLookupTree.create - from folder', 
  () => FileLookupTree.create(paths.folder)
);

await measureAsync(
  'FileLookupTree.create - from file list', 
  () => FileLookupTree.create([paths.file])
);

await measureAsync(
  'FileLookupTree.save', 
  () => FileLookupTree.save(paths.fileLookupTreeSave, fileLookupTree)
);

await measureAsync(
  'FileLookupTree.load', 
  () => FileLookupTree.load(paths.fileLookupTreeSave)
);

await measureAsync(
  'FileLookupTree.add', 
  () => FileLookupTree.add(fileLookupTree, paths.file)
);

await measureAsync(
  'FileLookupTree.remove', 
  () => FileLookupTree.remove(fileLookupTree, paths.file)
);

await measureAsync(
  'FileLookupTree.exists', 
  () => FileLookupTree.exists(fileLookupTree, fileIdentifier)
);

console.log(`\n\x1b[30m\x1b[42m Additional information: \x1b[0m\n`);
console.log(`\x1b[92mTarget file size is:\x1b[0m ${getFileSize(paths.file)}`);
console.log(`\x1b[92mTarget folder size is:\x1b[0m ${await getFolderSize(paths.folder)}`);
console.log(`\x1b[92mFile Lookup Tree save size is:\x1b[0m ${getFileSize(paths.fileLookupTreeSave)}`);
console.log(`\x1b[92mFile Lookup Tree saved in:\x1b[0m ${dirname(paths.fileLookupTreeSave)}${sep}\x1b[96m${basename(paths.fileLookupTreeSave)}\x1b[0m`);
console.log('Ran all performance tests.');