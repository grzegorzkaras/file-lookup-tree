'use strict';

import { resolve } from 'path';
import { statSync, existsSync } from 'fs';
import { PerformanceObserver, performance } from 'perf_hooks';

import * as R from 'ramda';
import { list as recursiveReaddir } from 'recursive-readdir-async';

export const getArgumentFilepath = () => {
  const filepath = process.argv[2] ? resolve(process.argv[2]) : null;

  if (!filepath || !existsSync(filepath) || !statSync(filepath).isFile()) {
    console.error('Script must be given an argument with a file path, that will be tested against the directory this file is in.')
    process.exit(1);
  }

  return filepath;
};

export const setupBenchmark = () => {
  new PerformanceObserver(R.compose(
    console.log,
    ({ name, ms, time }) => `\x1b[92mTest:\x1b[0m ${ms} \x1b[96mms\x1b[0m\t\t${time}\t\t${name}`,
    R.head,
    R.map(R.applySpec({
      name: R.compose(R.replace(/\./g, '\x1b[96m.\x1b[0m'), R.prop('name')),
      ms: R.prop('duration'),
      time: R.compose(
        ({ hours, minutes, seconds }) => `${hours} \x1b[96mh\x1b[0m ${minutes} \x1b[96mmin\x1b[0m ${seconds} \x1b[96ms\x1b[0m`,
        R.map(value => `${value < 10 ? ` ${value}` : value}`),
        R.map(Math.floor),
        R.applySpec({
          hours: ({ duration }) => (duration / 3600000) % 24,
          minutes: ({ duration }) => (duration / 60000) % 60,
          seconds: ({ duration }) => (duration / 1000) % 60
        })
      )
    })),
    R.invoker(0, 'getEntries')
  )).observe({ entryTypes: ['measure'], buffer: true });
};

export const measureAsync = async (name, fn) => {
  performance.mark(`${name} start`);

  const result = await fn();

  performance.mark(`${name} finish`);
  performance.measure(name, `${name} start`, `${name} finish`);

  return result;
};

const formatBytes = (bytes, decimals = 2) => {
  if (bytes === 0) return '0 Bytes';

  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(1024));

  return `${parseFloat((bytes / Math.pow(1024, i)).toFixed(decimals))} \x1b[96m${sizes[i]}\x1b[0m`;
};

export const getFolderSize = R.compose(
  R.andThen(R.compose(
    formatBytes,
    R.reduce(R.add, 0),
    R.map(R.prop('size')),
    R.map(statSync),
    R.map(R.prop('fullname'))
  )),
  R.unary(recursiveReaddir)
);

export const getFileSize = R.compose(formatBytes, R.prop('size'), statSync);