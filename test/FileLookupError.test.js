'use strict';

import { FileLookupError } from '../lib/FileLookupError.js';

describe('FileLookupError', () => {
  describe('throw', () => {
    it('should throw with message and data object', () => {
      try {
        throw new FileLookupError('error message', { a: 1, b: 2, c: 3 });
      } catch (ex) {
        expect(ex.message).toBe('error message');
        expect(ex.data).toEqual({ a: 1, b: 2, c: 3 });
      }
    });
  });
});