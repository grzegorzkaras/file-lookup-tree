'use strict';

import { resolve } from 'path';
import { readFile } from 'fs/promises';

import * as FileIdentifier from '../lib/FileIdentifier.js';

describe('FileIdentifier', () => {
  const filePath = resolve('./test/files/new-file.txt');

  describe('create', () => {
    it('should create a new FileIdentifier returning size, hash and content of a file in a sequence', async () => {
      const fileIdentifier = FileIdentifier.create(filePath);

      const fileSize = await FileIdentifier.getNextProperty(fileIdentifier);
      const fileHash = await FileIdentifier.getNextProperty(fileIdentifier);
      const fileContent = await FileIdentifier.getNextProperty(fileIdentifier);

      expect(fileSize).toBe('16');
      expect(fileHash).toBe('f2f0509f347fa292d14cc771cb59f2c2');
      expect(fileContent.equals(await readFile(filePath))).toBe(true);
    });
  });

  describe('getNextProperty', () => {
    it('should get next property of a iterator', async () => {
      const iteratorNextMock = jest.fn();

      iteratorNextMock.mockReturnValue({ value: 'next property' });

      const nextProperty = FileIdentifier.getNextProperty({ next: iteratorNextMock });

      expect(iteratorNextMock).toHaveBeenCalledWith();
      expect(nextProperty).toBe('next property');
    });
  });
});