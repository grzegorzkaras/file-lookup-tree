'use strict';

import { resolve } from 'path';

import * as R from 'ramda';
import { list as recursiveReaddir } from 'recursive-readdir-async';
import { readFile, writeFile } from 'atomically';

import { getSize, getHash, getContent } from './file.js';
import { stringify, andThenMultiple } from './utils/functional.js';
import { FileLookupError } from './FileLookupError.js';
import * as FileIdentifier from './FileIdentifier.js';

/**
 * Creates a new FileLookupEntry object.
 * 
 * @function
 * @typedefn FileLookupEntry = Object
 * @sig String -> Promise
 * @param {String} path A path to a file.
 * @returns {Promise<FileLookupEntry>} A promise resolving with a FileLookupEntry.
 */
const createLookupEntry = async path => ({ 
  [await getSize(path)]: { 
    [await getHash(path)]: [path] 
  } 
});

/**
 * Creates FileLookupTree from an array of file paths.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig [String] -> Promise
 * @param {Array<String>} paths An array of file paths.
 * @returns {Promise<FileLookupTree>} A promise resolving with a FileLookupTree.
 */
const createLookupTreeFromArray = R.compose(
  R.andThen(R.reduce(R.mergeDeepWith(R.concat), {})),
  R.bind(Promise.all, Promise),
  R.map(createLookupEntry),
  R.map(resolve)
);

/**
 * Creates FileLookupTree from a path to a folder and its subfolders.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig String -> Promise
 * @param {String} path A path to a folder.
 * @returns {Promise<FileLookupTree>} A promise resolving with a FileLookupTree.
 */
const createLookupTreeFromFolderPath = R.compose(
  R.andThen(createLookupTreeFromArray), 
  R.andThen(R.map(R.prop('fullname'))),
  recursiveReaddir
);

/**
 * Chooses FileLookupTree creator function based on the argument type. 
 * 
 * In the case of a string, it will create FileLookupTree from a provided folder path.  
 * In the case of an array, it will create FileLookupTree from a provided array of file paths.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig String|[String] -> Promise
 * @param {String|Array<String>} argument A path to a folder or an array of paths to files.
 * @returns {Promise<FileLookupTree>} A promise resolving with a FileLookupTree.
 */
export const create = R.cond([
  [R.is(String), createLookupTreeFromFolderPath],
  [R.is(Array), createLookupTreeFromArray]
]);

/**
 * Saves data to a provided file path with measures to prevent an existing file from becoming corrupted, in case of the writing process
 * being interrupted.
 * 
 * Works by creating a temporary file first, that once written, has its name changed to that of the target file.
 * 
 * In case of interruption, the temporary file will be kept on the drive.
 * 
 * @function
 * @sig String -> String -> Promise
 * @param {String} path A path to a file.
 * @param {String} data A text to be written into a file.
 * @returns {Promise} A promise resolving once the file writing is accomplished.
 */
const writeFileWithTemp = R.partialRight(writeFile, [{ tmpPurge: false }]);

/**
 * Saves a FileLookupTree in form of a JSON file to a given path.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig String -> FileLookupTree -> Promise
 * @param {String} path A path pointing to a location of the save file.
 * @param {FileLookupTree} fileLookupTree A FileLookupTree to be saved.
 * @returns {Promise} A promise resolving once a FileLookupTree has been saved to a file.
 */
export const save = R.useWith(
  writeFileWithTemp, [
    R.identity, 
    stringify
  ]
);

/**
 * Loads a FileLookupTree from a JSON file to a given path.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig String -> Promise
 * @param {String} path A path pointing to a location of the save file.
 * @returns {Promise<FileLookupTree>} A promise resolving once a FileLookupTree has been loaded from a file. Resolves with the FileLookupTree.
 */
export const load = R.compose(
  R.andThen(JSON.parse), 
  readFile
);

/**
 * Adds a new file to an existing FileLookupTree.
 * 
 * This function will not check for the duplicates of files in the FileLookupTree.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig FileLookupTree -> String -> Promise
 * @param {FileLookupTree} fileLookupTree A FileLookupTree to be modified.
 * @param {String} path A path to a new file.
 * @returns {Promise<FileLookupTree>} A promise resolving with a modified FileLookupTree.
 */
export const add = R.useWith(
  andThenMultiple(R.mergeDeepWith(R.concat)), [
    R.identity, 
    R.compose(createLookupEntry, resolve)
  ]
);

/**
 * Removes size objects from FileLookupTree if their value is an empty hashes object.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig FileLookupTree -> FileLookupTree
 * @param {FileLookupTree} fileLookupTree A FileLookupTree to be modified.
 * @returns {FileLookupTree} A modified FileLookupTree.
 */
const removeEmptySizes = R.reject(R.isEmpty);

/**
 * Removes hash objects from FileLookupTree if their value is an empty paths array.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig FileLookupTree -> FileLookupTree
 * @param {FileLookupTree} fileLookupTree A FileLookupTree to be modified.
 * @returns {FileLookupTree} A modified FileLookupTree.
 */
const removeEmptyHashes = R.map(R.reject(R.isEmpty));

/**
 * Filters the path arrays in the FileLookupTree, removing the provided path from them.
 * 
 * If the path is found in an array with a single item, it leaves an empty array.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig FileLookupTree -> String -> FileLookupTree
 * @param {FileLookupTree} fileLookupTree A FileLookupTree to be modified.
 * @param {String} path A file path to be removed from FileLookupTree.
 * @returns {FileLookupTree} A modified FileLookupTree.
 */
const removePath = R.useWith(
  R.flip(R.map), [
    R.identity, 
    R.compose(R.map, R.reject, R.equals, resolve)
  ]
);

/**
 * Removes a FileLookupEntry from existing FileLookupTree using a file path.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @sig FileLookupTree -> String -> FileLookupTree
 * @param {FileLookupTree} fileLookupTree A FileLookupTree object to be modified.
 * @param {String} path A file path to be removed from FileLookupTree.
 * @returns {FileLookupTree} A modified FileLookupTree.
 */
export const remove = R.compose(
  removeEmptySizes,
  removeEmptyHashes,
  removePath
);

/**
 * Checks if provided file exists in a FileLookupTree.
 * 
 * @function
 * @typedefn FileLookupTree = Object
 * @typedefn FileIdentifier = Iterator
 * @sig FileLookupTree -> FileIdentifier -> Promise
 * @param {FileLookupTree} lookupTree A FileLookupTree to be searched through.
 * @param {FileIdentifier} fileIdentifier A FileIdentifier representing file to be found.
 * @returns {Promise<Boolean>} A promise resolving with `true` if the file exists in the FileLookupTree or `false` otherwise.
 * @throws {FileLookupError} Thrown in a situation when a file located in the FileLookupTree can't be loaded to have its contents read.
 *     It most likely means that the file no longer exists in the recorded location, 
 *     its name has been changed or there is an issue with the access permissions.
 */
export const exists = R.curry(async (lookupTree, fileIdentifier) => {
  const fileSize = await FileIdentifier.getNextProperty(fileIdentifier);
  const sizeChunk = lookupTree[fileSize];

  if (!sizeChunk) {
    return false;
  }

  const fileHash = await FileIdentifier.getNextProperty(fileIdentifier);
  const hashChunk = sizeChunk[fileHash];

  if (!hashChunk) {
    return false;
  }
  
  const fileContent = await FileIdentifier.getNextProperty(fileIdentifier);
    
  for (const path of hashChunk) {
    try {
      const recordFileContent = await getContent(path);
      
      if (fileContent.equals(recordFileContent)) {
        return true;
      }
    } catch (ex) {
      const data = {
        size: fileSize, 
        hash: fileHash, 
        path: path,
        originalError: ex
      };

      throw new FileLookupError(`file lookup tree entry: ${JSON.stringify(data, null, '  ')} is inaccessible.`, data);
    }
  }

  return false;
});
