'use strict';

/**
 * FileLookupError class, used to inform about file lookup-related exceptions.
 */
class FileLookupError extends Error {
  /**
   * Creates a FileLookupError instance.
   * 
   * @param {String} message An error message.
   * @param {Object} data An object containing additional information about the error.
   */
  constructor(message, data) {
    super(message);
    this.name = this.constructor.name;
    this.data = data;
  }
}

export { FileLookupError };