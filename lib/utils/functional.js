'use strict';

import * as R from 'ramda';

/**
 * Splits array into an object containing `head`, `body`, and `tail` elements,
 * with the `head` being the first element of the array, the `tail` being the last one 
 * and the `body` being the remaining elements from the middle of the array.
 *
 * @function
 * @sig [*] -> { head: *, body: [*], tail: * }
 * @param {Array} list An array of elements to be split.
 * @return {Object} An object containing first, last and remaining elements.
 */
const dissect = R.applySpec({
  head: R.head,
  body: R.compose(R.init, R.tail),
  tail: R.last
});

/**
 * Creates a generator function, which upon calling with any number of arguments, will return an iterator,
 * that will call a previously provided array of functions with those arguments, evaluating them only when 
 * their return value is needed (in the next iteration of the iterator).
 * 
 * The functions are evaluated from left to right, with the first function being called on the first `next()`
 * call of the iterator, and the last function ending the iteration (iterator `done` will be set to `true`).
 * 
 * @function
 * @sig [((a, b, ..., n) -> x), ((a, b, ..., n) -> y), ..., ((a, b, ..., n) -> z)] -> ((a, b, ..., n) -> Iterator)
 * @param {Array<Function>} functions An array of the functions to call. 
 *     Each taking all arguments provided to generator function, 
 *     evaluating them and yielding returned value on next iteration.
 * @return {Generator} A generator function taking any amount of arguments and returning iterator.
 */
export const lazySequencer = functions => function*(...args) {
  const { head, body, tail } = dissect(functions);

  for (const fn of [head, ...body]) {
    yield fn(...args)
  }

  return tail(...args);
};

/**
 * Stringifies an object with an indentation of two spaces, providing a more easily readable
 * object representation.
 * 
 * Equivalent to `data => JSON.stringify(data, null, '  ')`.
 * 
 * @function
 * @sig Object -> String
 * @param {Object} data An object to be stringified.
 * @return {String} A stringified object with an indentation of two spaces.
 */
export const stringify = R.partialRight(JSON.stringify, [null, '  ']);

/**
 * Returns the result of applying the variadic `onSuccess` function with the values obtained 
 * from an array of resolved promises.
 * 
 * @function
 * @sig ((a, b, ..., n) -> z) -> [Promise e a, Promise e b, ..., Promise e n] -> Promise e z
 * @sig ((a, b, ..., n) -> (Promise e z)) -> [Promise e a, Promise e b, ..., Promise e n] -> Promise e z 
 * @param {Function} onSuccess The function to apply. Can take multiple arguments and return a value or a promise of a value.
 * @param {Array<Promise>} promiseList An array of promises to resolve.
 * @return {Promise} A result of calling `onSuccess` with the values resolved from the `promiseList`.
 */
export const andThenMultiple = onSuccess => R.unapply(
  R.compose(
    R.andThen(
      R.apply(onSuccess)
    ), 
    R.bind(Promise.all, Promise)
  )
);