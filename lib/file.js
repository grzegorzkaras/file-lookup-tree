'use strict';

import { createReadStream } from 'fs';
import { stat, readFile } from 'fs/promises';
import { createHash } from 'crypto';

import * as R from 'ramda';

/**
 * Returns size of a file.
 * 
 * @function
 * @sig String -> Promise
 * @param {String} path A path to a file.
 * @return {Promise<String>} A promise resolving with a string containing size in bytes.
 */
export const getSize = R.compose(
  R.andThen(R.toString),
  R.andThen(R.prop('size')), 
  R.partialRight(stat, [{ bigint: true }])
);

/**
 * Calculates and returns MD5 hash of a file.
 * 
 * @function
 * @sig String -> Promise
 * @param {String} path A path to a file.
 * @return {Promise<String>} A promise resolving with a hash based on the file contents.
 */
export const getHash = path => new Promise((resolve, reject) => {
  const input = createReadStream(path);
  const output = createHash('md5');

  input.on('error', R.unary(reject));
  output.once('readable', () => resolve(output.read().toString('hex')));

  input.pipe(output);
});

/**
 * Reads and returns contents of a file.
 * 
 * @function
 * @sig String -> Promise
 * @param {String} path A path to a file.
 * @return {Promise<Buffer>} A promise resolving with a buffer containing the binary contents of a file.
 */
export const getContent = R.unary(readFile);