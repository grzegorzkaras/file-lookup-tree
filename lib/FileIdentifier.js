'use strict';

import { resolve } from 'path';

import * as R from 'ramda';

import { getSize, getHash, getContent } from './file.js';
import { lazySequencer } from './utils/functional.js';

/**
 * Creates a FileIdentifier iterator, which with every iteration will return a different property of a file.
 * 
 * On the first iteration, it will return a promise, that resolves with a string representing the size of the file in bytes.
 * On the second iteration, it will return a promise, that resolves with a string representing the MD5 hash of the file.
 * On the last iteration, it will return a promise, that resolves with a buffer containing the binary contents of the file.
 * 
 * These properties of a file are only obtained when needed, on the `next()` call. Because in many use cases of this library
 * it's not necessary to perform binary comparison of two files, and even the generation of a hash might not be needed,
 * the lazy evaluation of file properties can potentially save a lot of time, by avoiding unnecessary processing and loading of 
 * the file contents.
 * 
 * @function
 * @typedefn FileIdentifier = Iterator
 * @sig String -> FileIdentifier
 * @param {String} path A path to a file.
 * @return {FileIdentifier} An iterator retrieving information about the file in a sequence.
 */
export const create = R.compose(
  lazySequencer([getSize, getHash, getContent]), 
  resolve
);

/**
 * Gets the next property of a file from a FileIdentifier. 
 * The property returned is a promise, resolving with corresponding new information about the file.
 * 
 * @function
 * @typedefn FileIdentifier = Iterator
 * @sig FileIdentifier -> Promise
 * @param {FileIdentifier} iterator A FileIdentifier iterator.
 * @return {Promise<String>} A promise resolving with a file property.
 */
export const getNextProperty = R.compose(
  R.prop('value'),
  R.invoker(0, 'next')
);