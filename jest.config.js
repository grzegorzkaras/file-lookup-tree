export default {
  verbose: true,
  clearMocks: true,
  coverageDirectory: "test/coverage",
  coverageProvider: "babel",
  testEnvironment: "node",
  collectCoverage: true,
  moduleFileExtensions: [
    "js",
    "mjs"
  ],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.mjs$": "babel-jest"
  }
};
