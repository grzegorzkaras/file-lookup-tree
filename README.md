# File Lookup Tree &middot; [![Bitbucket license](https://img.shields.io/badge/license-MIT-blue.svg)](https://bitbucket.org/grzegorzkaras/file-lookup-tree/src/master/LICENSE)

File Lookup Tree is a Node.js library used for reliable and quick verification if a given file exists in the history of past files. 

Designed to check whenever a newly-downloaded file has already been downloaded before.

Uses the file information like the size, hash, and binary contents (compared only when the size and hash of a new file exist in the history), instead of the name of the file alone, avoiding an unnecessary waste of storage in form of duplicate files of different names.

The library relies on internal history (lookup tree data structure), that can be saved and loaded from a file. It doesn't track files that exist in the history, therefore changes in those files' location, name or contents can introduce incorrect results and errors. 

## Functionality
- Creation of persistent history of files, rather than relying on the current state of the storage.
- Performant verification of potential duplicate files with avoidance of unnecessary binary comparison of a file with the files from the history.
- Ability to reliably and asynchronously save and load the history on a drive.
- Only compares file contents if the size and hashes of both files are equal.

## Usage
```js
import { FileLookupTree, FileIdentifier } from 'file-lookup-tree';

const fileLookupTree = await FileLookupTree.create('/path/to/directory/'); 

/* Or:
const fileLookupTree = await FileLookupTree.create([
  '/path/to/directory/file-a.txt', 
  '/path/to/directory/file-b.txt', 
  '/path/to/directory/file-c.txt'
]);
*/

/* Or:
const fileLookupTree = await FileLookupTree.load('/path/to/save.json');
*/

const fileIdentifier = await FileIdentifier.create('path/to/directory/file-to-check.txt');

const fileToCheckExist = await FileLookupTree.exists(fileLookupTree, fileIdentifier);

if (fileToCheckExist) {
  await FileLookupTree.remove(fileLookupTree, 'path/to/directory/file-to-check.txt');
} else {
  await FileLookupTree.add(fileLookupTree, 'path/to/directory/file-to-check.txt');
}

await FileLookupTree.save('/path/to/save.json', fileLookupTree);
```

## Testing
- `yarn test` run tests.
- `yarn benchmark:default` run speed test with default path of files used in testing.
- `yarn benchmark ./path/to/directory/file.ext` run speed test of finding the given file in the directory it's located in.

## How does it work?

The usage of the library starts with giving it the information about the existing file locations from which the lookup tree should be created, or loading of an existing history from a file.

For each file, its size, the hash generated based on the contents of the file as well as the location of the file itself is saved to a single nested javascript object representing the tree, with keys being the information about the file, and the final value being an array of paths to the files with given size and hash.

Providing only a single file location will result in creating a tree of the following structure:
```json
{
  "11": {
    "0956d2fbd5d5c29844a4d21ed2f76e0c": [
      "full/path/to/directory/lorem-ipsum.txt"
    ]
  }
}
```

Subsequently, adding another file with completely different characteristics will add this information to the outermost object:
```json
{
  "11": {
    "0956d2fbd5d5c29844a4d21ed2f76e0c": [
      "full/path/to/directory/lorem-ipsum.txt"
    ],
  },
  "16": {
    "f2f0509f347fa292d14cc771cb59f2c2": [
      "full/path/to/directory/new-file.txt"
    ]
  }
}
```

However, if a newly added file has the same size as other already existing in the tree file, the object will merge:
```json
{
  "11": {
    "0956d2fbd5d5c29844a4d21ed2f76e0c": [
      "full/path/to/directory/lorem-ipsum.txt"
    ],
    "9c77a5f0be3981027d7e20e8d386e1bd": [
      "full/path/to/directory/lorem-ipsum-reversed.txt"
    ]
  }
}
```

The same principle applies in the case of both, the hash and the size, being the same (a scenario of a hash collision):
```json
{
  "11": {
    "0956d2fbd5d5c29844a4d21ed2f76e0c": [
      "full/path/to/directory/lorem-ipsum.txt",
      "full/path/to/directory/lorem-ipsum-copy.txt"
    ]
  }
}
```

To check whenever a given file exists in the history, a FileIdentifier has to be created:

FileIdentifier is an iterator, calling the `next()` method will sequentially result in `value` property being:
1. The size of the file
2. The hash of the file
3. The buffer of file data
```js
const fileIdentifier = await FileIdentifier.create('path/to/directory/lorem-ipsum.txt');

fileIdentifier.next().value // Size
fileIdentifier.next().value // Hash
fileIdentifier.next().value // Buffer
```

These values are obtained sequentially, only when `next()` is called, the obtaining of corresponding information occurs. 

### License

File Lookup Tree is [MIT licensed](./LICENSE).